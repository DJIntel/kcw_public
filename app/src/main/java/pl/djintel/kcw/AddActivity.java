package pl.djintel.kcw;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import java.io.File;

class AddActivity extends AppCompatActivity {

    private File f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        f = (File) this.getIntent().getSerializableExtra("f");
    }

    public void add(View v){
        Intent i = new Intent(AddActivity.this, MainActivity.class);
        String t = ((EditText)findViewById(R.id.t)).getText().toString();
        String m = ((EditText)findViewById(R.id.m)).getText().toString();
        String e = ((EditText)findViewById(R.id.editText)).getText().toString();
        boolean er = false;
        if (!t.matches(".*\\w.*")){ Toast.makeText(AddActivity.this, "Tytuł nie może być pusty!", Toast.LENGTH_SHORT).show(); er = true; }
        else if (t.length() < Config.MIN_TITLE_LENGTH || t.length() > Config.MAX_TITLE_LENGTH){ Toast.makeText(AddActivity.this, "Tytuł musi mieć pomiędzy " + Config.MIN_TITLE_LENGTH + " a " + (Config.MAX_TITLE_LENGTH - 1) + " znaków!", Toast.LENGTH_SHORT).show(); er = true; }
        else if (!m.matches(".*\\w.*")){ Toast.makeText(AddActivity.this, "Miejsce nie może być puste!", Toast.LENGTH_SHORT).show(); er = true; }
        else if (m.length() < Config.MIN_LOCATION_LENGTH || m.length() > Config.MAX_LOCATION_LENGTH){ Toast.makeText(AddActivity.this, "Miejsce musi mieć pomiędzy " + Config.MIN_LOCATION_LENGTH + " a " + (Config.MAX_LOCATION_LENGTH - 1) + " znaków!", Toast.LENGTH_SHORT).show(); er = true; }
        else if (!e.matches(".*\\w.*")){ Toast.makeText(AddActivity.this, "Opis nie może być pusty!", Toast.LENGTH_SHORT).show(); er = true; }
        else if (e.length() < Config.MIN_DESC_LENGTH || e.length() > Config.MAX_DESC_LENGTH){ Toast.makeText(AddActivity.this, "Opis musi mieć pomiędzy " + Config.MIN_DESC_LENGTH + " a " + (Config.MAX_DESC_LENGTH - 1) + " znaków!", Toast.LENGTH_SHORT).show(); er = true; }
        if(!er) {
            i.putExtra("t", t);
            i.putExtra("m", m);
            i.putExtra("e", e);
            i.putExtra("f", f);
            setResult(RESULT_OK, i);
            finish();
        }
    }
}
