package pl.djintel.kcw;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Vibrator;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

class RVAdapter extends RecyclerView.Adapter<RVAdapter.PhotosViewHolder>{

    private enum Who{
        w, p
    }

    private List<MainActivity.Photo> photos;
    private PhotosViewHolder[] photosViewHolder;
    private ProgressDialog d;

    RVAdapter(List<MainActivity.Photo> photos){
        this.photos = photos;
        this.photosViewHolder = new PhotosViewHolder[photos.size()];
    }

    @Override
    public PhotosViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card, viewGroup, false);
        return new PhotosViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final PhotosViewHolder photosViewHolder, int i) {
        final int j = i;
        this.photosViewHolder[i] = photosViewHolder;
        photosViewHolder.title.setText(photos.get(i).title);
        photosViewHolder.date.setText(getDate(Long.parseLong(photos.get(i).date) * 1000));
        photosViewHolder.location.setText(photos.get(i).location);
        photosViewHolder.desc.setText(photos.get(i).desc);

        try {
            File f=new File(photos.get(i).photo);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            photosViewHolder.photo.setImageBitmap(b);
        } catch (FileNotFoundException e) {
            photosViewHolder.photo.setImageResource(R.mipmap.ic_launcher);
            e.printStackTrace();
        }

        if(photos.get(i).p.equals("1")) photosViewHolder.loveP.setImageResource(R.drawable.hearth_loved);
        else photosViewHolder.loveP.setImageResource(R.drawable.ic_hearth);
        if(photos.get(i).w.equals("1")) photosViewHolder.loveW.setImageResource(R.drawable.hearth_loved);
        else photosViewHolder.loveW.setImageResource(R.drawable.ic_hearth);

        photosViewHolder.loveW.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(photos.get(j).w.equals("1")) lovedPhoto(j, Who.w, "0");
                else lovedPhoto(j, Who.w, "1");
            }

        });

        photosViewHolder.loveP.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(photos.get(j).p.equals("1")) lovedPhoto(j, Who.p, "0");
                else lovedPhoto(j, Who.p, "1");
            }

        });

        photosViewHolder.rl.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                try {
                    ContextWrapper cw = new ContextWrapper(photosViewHolder.cv.getContext());
                    File directory = cw.getDir("img_id", Context.MODE_PRIVATE);
                    File mypath = new File(directory + File.separator + photos.get(j).id + ".jpg");
                    copy(mypath, Environment.getExternalStorageDirectory().getPath() + "/KCW", photos.get(j).title + ".jpg");
                }catch (IOException e){
                    e.printStackTrace();
                    return;
                } catch (CopyingFileException e){
                    Toast.makeText(photosViewHolder.cv.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(photosViewHolder.cv.getContext(), "Zapisano \"" + photos.get(j).title + "\" w " + Environment.getExternalStorageDirectory().getPath() + "/KCW/" + photos.get(j).title + ".jpg!", Toast.LENGTH_LONG).show();
            }
        });

        photosViewHolder.rl.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Vibrator vibe = (Vibrator) photosViewHolder.cv.getContext().getSystemService(Context.VIBRATOR_SERVICE);
                vibe.vibrate(20);
                AlertDialog dialog = new AlertDialog.Builder(photosViewHolder.cv.getContext())
                        .setTitle("Edycja")
                        .setMessage("Co chcesz zrobić z \"" + photos.get(j).title + "\"?")
                        .setNeutralButton("Usunąć", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                delete(j);
                                dialog.dismiss();
                            }
                        }).setPositiveButton("Edytować", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                edit(j);
                            }
                        }).setNegativeButton("Nic", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create();
                dialog.show();
                return false;
            }
        });
    }

    private void edit(int i){
        Intent in = new Intent(photosViewHolder[i].cv.getContext(), EditActivity.class);
        in.putExtra("t", photos.get(i).title);
        in.putExtra("d", photos.get(i).date);
        in.putExtra("m", photos.get(i).location);
        in.putExtra("e", photos.get(i).desc);
        in.putExtra("f", photos.get(i).id);
        in.putExtra("w", photos.get(i).w);
        in.putExtra("p", photos.get(i).p);
        photosViewHolder[i].cv.getContext().startActivity(in);
    }

    private void delete(int i){
        d = new ProgressDialog(photosViewHolder[i].cv.getContext());
        d.setTitle("Usuwanie");
        d.setMessage("Usuwanie zdjęcia z serwera.");
        d.setIcon(R.drawable.ic_delete_sweep_white_24dp);
        d.show();
        new deleteTask().execute(i);
    }

    private class deleteTask extends AsyncTask<Integer, Void, Integer>{

        @Override
        protected Integer doInBackground(Integer... params) {
            FTPClient client = new FTPClient();

            try {
                client.connect(Config.FTP_HOST);
                client.login(Config.FTP_USER, Config.FTP_PASS);
                client.enterLocalPassiveMode();
                client.setFileType(FTP.BINARY_FILE_TYPE);
                client.deleteFile("imgs/"+photos.get(params[0]).id+".jpg");
                Log.i("dfsdf", "imgs/"+photos.get(params[0]).id+".jpg");
                client.logout();
                client.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return params[0];
        }

        @Override
        protected void onPostExecute(Integer s) {
            removed(s);
        }
    }

    private void removed(int i){
        d.dismiss();
        AlertDialog ad = new AlertDialog.Builder(photosViewHolder[i].cv.getContext())
                .setTitle("Usunięto")
                .setIcon(R.drawable.ic_delete_sweep_white_24dp)
                .setMessage("Usunięto zdjęcie \""+ photos.get(i).title + "\"!")
                .create();
        ad.show();
        photos.remove(i);
        notifyItemRemoved(i);
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    static class PhotosViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout rl;
        CardView cv;
        TextView title;
        TextView date;
        TextView location;
        TextView desc;
        ImageView photo;
        ImageView loveW;
        ImageView loveP;

        PhotosViewHolder(final View itemView) {
            super(itemView);
            rl = (RelativeLayout)itemView.findViewById(R.id.rl);
            cv = (CardView)itemView.findViewById(R.id.cv);
            title = (TextView)itemView.findViewById(R.id.title);
            date = (TextView)itemView.findViewById(R.id.date);
            location = (TextView) itemView.findViewById(R.id.location);
            desc = (TextView) itemView.findViewById(R.id.desc);
            photo = (ImageView)itemView.findViewById(R.id.photo);
            loveW = (ImageView)itemView.findViewById(R.id.loveW);
            loveP = (ImageView)itemView.findViewById(R.id.loveP);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private void lovedPhoto(int i, Who w, String b){
        String props;
        if(w == Who.p) props = photos.get(i).title + "#" + photos.get(i).date + "#" + photos.get(i).location + "#" + photos.get(i).desc + "#" + photos.get(i).w + "#" + b;
        else props = photos.get(i).title + "#" + photos.get(i).date + "#" + photos.get(i).location + "#" + photos.get(i).desc + "#" + b + "#" + photos.get(i).p;
        new uploadTask().execute(photos.get(i).id, props, i, w+"#"+b);
    }

    private class uploadTask extends AsyncTask<Object, Object, String> {

        @Override
        protected String doInBackground(Object... params) {
            return uploadPicture((String)params[0], (String)params[1], (int)params[2], (String)params[3]);
        }

        @Override
        protected void onPostExecute(String result){
            String[] r = result.split("#");
            int i = Integer.parseInt(r[0]);
            switch(r[1]){
                case "w": //w
                    /*if(r[2].equals("1")) photosViewHolder[i].loveW.setImageResource(R.drawable.hearth_loved);
                    else photosViewHolder[i].loveW.setImageResource(R.drawable.ic_hearth);*/
                    photos.get(i).w = r[2];
                    Log.i("dsd", result);
                    break;

                case "p": //p
                    /*if(r[2].equals("1")) photosViewHolder[i].loveP.setImageResource(R.drawable.hearth_loved);
                    else photosViewHolder[i].loveP.setImageResource(R.drawable.ic_hearth);*/
                    photos.get(i).p = r[2];
                    Log.i("dsd", result);
                    break;
            }
            loved(i);
        }
    }

    private void loved(int i){
        notifyItemChanged(i);
    }

    private void copy(File src, String dst, String n) throws IOException, CopyingFileException{
        InputStream in = new FileInputStream(src);
        File fdst = new File(dst);

        boolean success = true;
        if (!fdst.exists()) {
            success = fdst.mkdir();
        }
        if (success) {
            fdst = new File(dst + "/" + n);
            OutputStream out = new FileOutputStream(fdst);
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        } else {
            throw new CopyingFileException("Nie można zapisać!");
        }
    }

    private String uploadPicture(String f, String props, int i, String wb){
        try {
            FTPClient client = new FTPClient();

            client.connect(Config.FTP_HOST);
            client.login(Config.FTP_USER, Config.FTP_PASS);
            client.enterLocalPassiveMode();
            client.setFileType(FTP.BINARY_FILE_TYPE);

            ContextWrapper cw = new ContextWrapper(photosViewHolder[i].cv.getContext());
            File directory = cw.getDir("props", Context.MODE_PRIVATE);
            File mypath = new File(directory + File.separator + f + ".kcw");
            FileOutputStream output = new FileOutputStream(mypath);
            output.write(props.getBytes());
            output.close();

            FileInputStream fis = new FileInputStream(mypath);
            client.storeFile("props/"+f+".kcw", fis);
            client.logout();
            client.disconnect();
            Log.i("ff", "11");

        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
        return i + "#" + wb;
    }

    private String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        return DateFormat.format("dd-MM-yyyy", cal).toString();
    }

    private class CopyingFileException extends Exception{
        CopyingFileException(String message) { super(message); }
    }
}
