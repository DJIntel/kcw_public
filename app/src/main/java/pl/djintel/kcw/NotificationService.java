package pl.djintel.kcw;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.v7.app.NotificationCompat;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class NotificationService extends Service {

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new startTask().execute();
        return START_NOT_STICKY;
    }

    private class startTask extends AsyncTask<String, Void, String>{

            @Override
            protected String doInBackground(String... params) {
                String[] IDs = getIDs();
                Vars.photos = new ArrayList<>();
                for (String ID : IDs) {
                    String props[] = getProps(ID).split("#");
                    try {
                        String title = props[0];
                        String date = props[1];
                        String location = props[2];
                        String desc = props[3];
                        String w = props[4];
                        String p = props[5];
                        Vars.photos.add(new Photo(ID, title, date, location, desc, w, p));
                        Collections.sort(Vars.photos, new Comparator<Photo>() {
                            @Override
                            public int compare(Photo o1, Photo o2) {
                                return o2.date.compareTo(o1.date);
                            }
                        });
                    } catch (ArrayIndexOutOfBoundsException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e){
                        e.printStackTrace();
                    }
                }
                return null;
            }

        @Override
        protected void onPostExecute(String s) {
            new NotifyTask().execute();
        }
    }

    private class NotifyTask extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... params) {
            try {
                ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                NetworkInfo mData = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
                if (mWifi.isConnected() || mData.isConnected()) {
                    while(true) {
                        String[] IDs = getIDs();
                        List<Photo> photos = new ArrayList<>();
                        for (String ID : IDs) {
                            String props[] = getProps(ID).split("#");
                            try {
                                String title = props[0];
                                String date = props[1];
                                String location = props[2];
                                String desc = props[3];
                                String w = props[4];
                                String p = props[5];
                                photos.add(new Photo(ID, title, date, location, desc, w, p));
                                Collections.sort(photos, new Comparator<Photo>() {
                                    @Override
                                    public int compare(Photo o1, Photo o2) {
                                        return o2.date.compareTo(o1.date);
                                    }
                                });
                            } catch (ArrayIndexOutOfBoundsException e) {
                                e.printStackTrace();
                            }
                        }
                        if (!Vars.photos.get(0).id.equals(photos.get(0).id) && !Vars.LAST_ID.equals(Vars.photos.get(0).id)) {
                            createNotification(photos.get(0).title);
                            Vars.LAST_ID = Vars.photos.get(0).id;
                        }
                        Thread.sleep(1000 * 10);
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private void createNotification(String title) {
        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);
        Notification noti;

        NotificationCompat.Builder mBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this).setSmallIcon(R.drawable.ic_hearth);
        mBuilder.setContentTitle("Nowe Zdjęcie!");
        mBuilder.setContentText("Nowe zdjęcie: \"" + title + "\"!!!");
        noti = mBuilder.build();

        noti.priority = Notification.PRIORITY_MAX;
        noti.contentIntent = pIntent;
        noti.vibrate = new long[]{200, 100, 200};

        NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotifyMgr.notify(1, noti);
    }

    private String getProps(String id) {
        try {
            URL url = new URL(Config.HOST + "/api.php?KEY=" + Config.API_KEY + "&void=2&ID=" + id);

            URLConnection con = url.openConnection();
            con.connect();
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuilder response = new StringBuilder();
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            String result[] = response.toString().split("@#@#@#");
            return result[result.length - 2];
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String[] getIDs() {
        try {
            URL url = new URL(Config.HOST + "/api.php?KEY=" + Config.API_KEY + "&void=1");
            URLConnection con = url.openConnection();
            con.connect();
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuilder response = new StringBuilder();
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            String result[] = response.toString().split("@#@#@#");
            return result[result.length - 2].split("###");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public class Photo{
        String id;
        String title;
        String date;
        String location;
        String desc;
        String w;
        String p;

        Photo(String id, String title, String date, String location, String desc, String w, String p) {
            this.id = id;
            this.title = title;
            this.date = date;
            this.location = location;
            this.desc = desc;
            this.w = w;
            this.p = p;
        }
    }
}
