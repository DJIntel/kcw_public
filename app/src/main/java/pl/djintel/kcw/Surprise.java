package pl.djintel.kcw;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;

import com.plattysoft.leonids.ParticleSystem;

class Surprise extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suprise);
    }

    public void shot(View v){
        try {
            new ParticleSystem(this, 15000, R.drawable.blue_hearth, 3000, R.id.frame)
                    .setSpeedRange(0.2f, 0.3f)
                    .setScaleRange(1.5f, 0.5f)
                    .setRotationSpeedRange(120, 360)
                    .oneShot(v, Vars.SUPRISE_BLUE);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private ParticleSystem ps;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                ps = new ParticleSystem(this, 1000, R.drawable.hearth_small, 1500);
                ps.setScaleRange(0.7f, 1.3f);
                ps.setSpeedRange(0.05f, 0.1f);
                ps.setRotationSpeedRange(90, 180);
                ps.setFadeOut(300, new AccelerateInterpolator());
                ps.emit((int) event.getX(), (int) event.getY(), Vars.SUPRISE_RED);
                break;
            case MotionEvent.ACTION_MOVE:
                ps.updateEmitPoint((int) event.getX(), (int) event.getY());
                break;
            case MotionEvent.ACTION_UP:
                ps.stopEmitting();
                break;
        }
        return true;
    }
}
