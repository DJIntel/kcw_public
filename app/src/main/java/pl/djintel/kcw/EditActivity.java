package pl.djintel.kcw;

import android.content.Context;
import android.content.ContextWrapper;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

class EditActivity extends AppCompatActivity {

    private String p, f, m, e, t, w, d;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        p = getIntent().getStringExtra("p");
        t = getIntent().getStringExtra("t");
        m = getIntent().getStringExtra("m");
        e = getIntent().getStringExtra("e");
        d = getIntent().getStringExtra("d");
        w = getIntent().getStringExtra("w");
        f = getIntent().getStringExtra("f");
        ((EditText) findViewById(R.id.t)).setText(t);
        ((EditText) findViewById(R.id.m)).setText(m);
        ((EditText) findViewById(R.id.editText)).setText(e);
    }

    public void edit(View v){
        t = ((EditText)findViewById(R.id.t)).getText().toString();
        m = ((EditText)findViewById(R.id.m)).getText().toString();
        e = ((EditText)findViewById(R.id.editText)).getText().toString();
        boolean er = false;
        if (!t.matches(".*\\w.*")){ Toast.makeText(EditActivity.this, "Tytuł nie może być pusty!", Toast.LENGTH_SHORT).show(); er = true; }
        else if (t.length() < Config.MIN_TITLE_LENGTH || t.length() > Config.MAX_TITLE_LENGTH){ Toast.makeText(EditActivity.this, "Tytuł musi mieć pomiędzy " + Config.MIN_TITLE_LENGTH + " a " + (Config.MAX_TITLE_LENGTH - 1) + " znaków!", Toast.LENGTH_SHORT).show(); er = true; }
        else if (!m.matches(".*\\w.*")){ Toast.makeText(EditActivity.this, "Miejsce nie może być puste!", Toast.LENGTH_SHORT).show(); er = true; }
        else if (m.length() < Config.MIN_LOCATION_LENGTH || m.length() > Config.MAX_LOCATION_LENGTH){ Toast.makeText(EditActivity.this, "Miejsce musi mieć pomiędzy " + Config.MIN_LOCATION_LENGTH + " a " + (Config.MAX_LOCATION_LENGTH - 1) + " znaków!", Toast.LENGTH_SHORT).show(); er = true; }
        else if (!e.matches(".*\\w.*")){ Toast.makeText(EditActivity.this, "Opis nie może być pusty!", Toast.LENGTH_SHORT).show(); er = true; }
        else if (e.length() < Config.MIN_DESC_LENGTH || e.length() > Config.MAX_DESC_LENGTH){ Toast.makeText(EditActivity.this, "Opis musi mieć pomiędzy " + Config.MIN_DESC_LENGTH + " a " + (Config.MAX_DESC_LENGTH - 1) + " znaków!", Toast.LENGTH_SHORT).show(); er = true; }
        if(!er) {
            String props = t + "#" + d + "#" + m + "#" + e + "#" + p + "#" + w;
            new uploadTask().execute(f, props);
        }
    }

    private class uploadTask extends AsyncTask<Object, Object, Integer> {

        @Override
        protected Integer doInBackground(Object... params) {
            return uploadPicture((String)params[0], (String)params[1]);
        }

        @Override
        protected void onPostExecute(Integer result){
            uploaded();
        }
    }

    private void uploaded(){
        //dialog.dismiss();
        finish();
    }

    private int uploadPicture(String f, String props){
        try {
            FTPClient client = new FTPClient();

            client.connect(Config.FTP_HOST);
            client.login(Config.FTP_USER, Config.FTP_PASS);
            client.enterLocalPassiveMode();
            client.setFileType(FTP.BINARY_FILE_TYPE);

            ContextWrapper cw = new ContextWrapper(getApplicationContext());
            File directory = cw.getDir("props", Context.MODE_PRIVATE);
            File mypath = new File(directory + File.separator + f + ".kcw");
            FileOutputStream output = new FileOutputStream(mypath);
            output.write(props.getBytes());
            output.close();

            FileInputStream fis = new FileInputStream(mypath);
            client.storeFile("props/"+f+".kcw", fis);
            client.logout();
            client.disconnect();
            Log.i("ff", "11");

        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
