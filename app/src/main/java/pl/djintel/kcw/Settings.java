package pl.djintel.kcw;

import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

class Settings extends AppCompatActivity {

    SeekBar seekBar, seekBar2;
    TextView seekBarValue, seekBarValue2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        seekBarValue = (TextView)findViewById(R.id.textView4);
        seekBar = (SeekBar)findViewById(R.id.seekBar);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int MIN = 30;
                if (progress <= MIN) {
                    seekBar.setProgress(MIN);
                    seekBarValue.setText(String.valueOf(MIN));
                }else{
                    seekBarValue.setText(String.valueOf(progress));
                }
            }
        });
        seekBar.setProgress(Vars.SUPRISE_BLUE);
        seekBarValue.setText(String.valueOf(Vars.SUPRISE_BLUE));

        //************** RED **************//

        seekBarValue2 = (TextView)findViewById(R.id.textView6);
        seekBar2 = (SeekBar)findViewById(R.id.seekBar2);
        seekBar2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int MIN = 15;
                if (progress <= MIN) {
                    seekBar.setProgress(MIN);
                    seekBarValue2.setText(String.valueOf(MIN));
                }else{
                    seekBarValue2.setText(String.valueOf(progress));
                }
            }
        });
        seekBar2.setProgress(Vars.SUPRISE_RED);
        seekBarValue2.setText(String.valueOf(Vars.SUPRISE_RED));
    }

    public void save(View v) {
        Vars.SUPRISE_BLUE = seekBar.getProgress();
        Vars.SUPRISE_RED = seekBar2.getProgress();
        try {
            ContextWrapper cw = new ContextWrapper(getApplicationContext());
            File directory = cw.getDir("config", Context.MODE_PRIVATE);

            //********** suprise blue ***********//
            File mypath = new File(directory + File.separator + "suprise_blue.txt");
            FileOutputStream output = new FileOutputStream(mypath);
            output.write(String.valueOf(seekBar.getProgress()).getBytes());
            output.close();

            //********** suprise red ***********//
            mypath = new File(directory + File.separator + "suprise_red.txt");
            output = new FileOutputStream(mypath);
            output.write(String.valueOf(seekBar2.getProgress()).getBytes());
            output.close();

            Toast.makeText(getApplicationContext(), "Zapisano!", Toast.LENGTH_SHORT).show();
            super.onBackPressed();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
