package pl.djintel.kcw;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.io.CopyStreamAdapter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class MainActivity extends AppCompatActivity implements View.OnClickListener {

    List<Photo> photos;
    SwipeRefreshLayout srl;
    FloatingActionMenu fabMenu;
    FloatingActionButton fabAdd;
    FloatingActionButton fabSettings;
    FloatingActionButton fab;
    ConnectivityManager connManager;
    ProgressDialog dialog;
    RecyclerView rv;
    Boolean c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);
        super.onCreate(savedInstanceState);

        loadSettings();
        connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        srl = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh_layout);
        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                get();
            }
        });
        srl.setColorSchemeResources(R.color.colorAccent);
        srl.setProgressBackgroundColorSchemeColor(Color.rgb(70, 70, 70));


        fabMenu = (FloatingActionMenu) findViewById(R.id.menu);
        fabAdd = (FloatingActionButton)findViewById(R.id.add_from_gallery_fab);
        fabSettings = (FloatingActionButton)findViewById(R.id.settings_fab);
        fab = (FloatingActionButton)findViewById(R.id.menu_item);
        fabAdd.setOnClickListener(this);
        fabSettings.setOnClickListener(this);
        fab.setOnClickListener(this);


        dialog = new ProgressDialog(MainActivity.this);
        dialog.setMessage("Wysyłanie obrazka na serwer.");
        dialog.setCancelable(false);
        dialog.setMax(100);
        dialog.setTitle("Wysyłanie!");
        dialog.setIcon(R.drawable.ic_file_upload_white_24dp);
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Anuluj", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                c=false;
            }
        });
    }

    @Override
    protected void onResume() {
        srl.setRefreshing(true);
        get();
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.add_from_gallery_fab:
                Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                getIntent.setType("image/*");
                Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                pickIntent.setType("image/*");
                Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});
                startActivityForResult(chooserIntent, 1);
                break;

            case R.id.settings_fab:
                startActivity(new Intent(MainActivity.this, Settings.class));
                break;

            case R.id.menu_item:
                startActivity(new Intent(MainActivity.this, Surprise.class));
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == 1) {
            Uri uri = data.getData();
            Log.d("fdf", "File Uri: " + uri.toString());
            File f = new File(uri.getPath());
            Log.d("dsfsd", "File Path: " + f.getAbsolutePath() + " Name: " + f.getName());
            startActivityForResult(new Intent(MainActivity.this, AddActivity.class).putExtra("f", f), 2);
        } else if(resultCode == RESULT_OK && requestCode == 2){
            File f = (File)data.getSerializableExtra("f");
            String t = data.getStringExtra("t");
            String m = data.getStringExtra("m");
            String e = data.getStringExtra("e");
            String p = t + "#" + System.currentTimeMillis()/1000 + "#" + m + "#" + e + "#0#0";
            dialog.show();
            c = true;
            new uploadTask().execute(f, p);
        } else if(resultCode == RESULT_OK && requestCode == 3){
            Log.i("xde", "" + data.getData());
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void loadSettings(){
        try {
            ContextWrapper cw = new ContextWrapper(getApplicationContext());
            File directory = cw.getDir("config", Context.MODE_PRIVATE);

            //*********** suprise_blue ************//
            File mypath = new File(directory + File.separator + "suprise_blue.txt");
            FileInputStream fis = new FileInputStream(mypath);
            StringBuilder builder = new StringBuilder();
            int ch;
            while((ch = fis.read()) != -1){
                builder.append((char)ch);
            }
            Vars.SUPRISE_BLUE = Integer.parseInt(builder.toString());


            //*********** suprise_blue ************//
            mypath = new File(directory + File.separator + "suprise_red.txt");
            fis = new FileInputStream(mypath);
            builder = new StringBuilder();
            while((ch = fis.read()) != -1){
                builder.append((char)ch);
            }
            Vars.SUPRISE_RED = Integer.parseInt(builder.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class uploadTask extends AsyncTask<Object, Object, Integer> {

        @Override
        protected Integer doInBackground(Object... params) {
            return uploadPicture((File)params[0], (String)params[1]);
        }

        @Override
        protected void onPostExecute(Integer result){
            uploaded();
        }
    }

    private void uploaded(){
        dialog.dismiss();
        get();
    }

    private int uploadPicture(final File f, String props){
        try {
            FTPClient client = new FTPClient();
            client.setCopyStreamListener(new CopyStreamAdapter() {
                @Override
                public void bytesTransferred(long totalBytesTransferred, int bytesTransferred, long streamSize) {
                    int percent = (int)(totalBytesTransferred*100/f.length());
                    dialog.setProgress(percent);
                }
            });

            client.connect(Config.FTP_HOST);
            client.login(Config.FTP_USER, Config.FTP_PASS);
            client.enterLocalPassiveMode();
            client.setFileType(FTP.BINARY_FILE_TYPE);

            String name = FilenameUtils.getBaseName(f.getName());

            FileInputStream fis = new FileInputStream(f);
            client.storeFile("imgs/"+name+".jpg", fis);
            fis.close();

            ContextWrapper cw = new ContextWrapper(getApplicationContext());
            File directory = cw.getDir("props", Context.MODE_PRIVATE);
            File mypath = new File(directory + File.separator + name + ".kcw");
            FileOutputStream output = new FileOutputStream(mypath);
            output.write(props.getBytes());
            output.close();

            fis = new FileInputStream(mypath);
            client.storeFile("props/"+name+".kcw", fis);
            client.logout();
            client.disconnect();
            Log.i("ff", "11");

        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private void initRv(){
        rv = (RecyclerView)findViewById(R.id.recyclerView);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);
        RVAdapter adapter = new RVAdapter(photos);
        rv.setAdapter(adapter);
    }

    public String getBitmapFromURL(String ID){
        try {
            String outputName = ID +".jpg";
            URL url = new URL(Config.HOST + "/api.php?ID=" + outputName + "&KEY=" + Config.API_KEY + "&void=0");
            Log.i("url", String.valueOf(url));
            InputStream input = url.openConnection().getInputStream();
            ContextWrapper cw = new ContextWrapper(getApplicationContext());
            File directory = cw.getDir("img_id", Context.MODE_PRIVATE);
            File mypath = new File(directory + File.separator + outputName);
            FileOutputStream output = new FileOutputStream(mypath);
            int read;
            byte[] data = new byte[10240];
            while ((read = input.read(data)) != -1){
                output.write(data, 0, read);
            }
            output.close();
            input.close();
            return mypath.getAbsolutePath();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String[] getIDs() {
        try {
            URL url = new URL(Config.HOST + "/api.php?KEY=" + Config.API_KEY + "&void=1");
            Log.i("url", String.valueOf(url));
            URLConnection con = url.openConnection();
            con.connect();
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuilder response = new StringBuilder();
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            String result[] = response.toString().split("@#@#@#");
            return result[result.length - 2].split("###");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getProps(String id) {
        try {
            URL url = new URL(Config.HOST + "/api.php?KEY=" + Config.API_KEY + "&void=2&ID=" + id);
            Log.i("url", String.valueOf(url));
            URLConnection con = url.openConnection();
            con.connect();
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuilder response = new StringBuilder();
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            String result[] = response.toString().split("@#@#@#");
            return result[result.length - 2];
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void get(){
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mData = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if(mWifi.isConnected() || mData.isConnected()){
            new GetTask().execute("");
        }else{
            srl.setRefreshing(false);
            Toast.makeText(MainActivity.this, "Nie można odświeżyć, brak połączenia z siecią!", Toast.LENGTH_LONG).show();
        }
    }

    private class GetTask extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... params) {
            String[] IDs = getIDs();
            photos = new ArrayList<>();
            for(int i = 0; i < IDs.length; i++) {
                Log.i("" + i, IDs[i]);
                String res = getBitmapFromURL(IDs[i]);
                String props[] = getProps(IDs[i]).split("#");
                try {
                    String title = props[0];
                    String date = props[1];
                    String location = props[2];
                    String desc = props[3];
                    String w = props[4];
                    String p = props[5];
                    photos.add(new Photo(IDs[i], title, date, location, desc,  res, w, p));
                }catch (ArrayIndexOutOfBoundsException e){
                    e.printStackTrace();
                }
            }
           return "";
        }

        @Override
        protected void onPostExecute(String result){
            Collections.sort(photos, new Comparator<Photo>() {
                @Override
                public int compare(Photo o1, Photo o2) {
                    return o2.date.compareTo(o1.date);
                }
            });
            Vars.LAST_ID = photos.get(photos.size()-1).id;
            initRv();
            srl.setRefreshing(false);
            findViewById(R.id.activity_mian).invalidate();
            Log.i("ope", "initRv();");
            if(!Vars.FIRST_REFRESH){ startService(new Intent(MainActivity.this, NotificationService.class)); Vars.FIRST_REFRESH = true; Log.i("NS", "Starting"); }
        }
    }

    public class Photo{
        String id;
        String title;
        String date;
        String location;
        String desc;
        String photo;
        String w;
        String p;

        Photo(String id, String title, String date, String location, String desc, String photo, String w, String p) {
            this.id = id;
            this.title = title;
            this.date = date;
            this.location = location;
            this.desc = desc;
            this.photo = photo;
            this.w = w;
            this.p = p;
        }
    }
}