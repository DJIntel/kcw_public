package pl.djintel.kcw;

class Config {
    static final int MIN_TITLE_LENGTH = 3;
    static final int MAX_TITLE_LENGTH = 21;
    static final int MIN_LOCATION_LENGTH = 4;
    static final int MAX_LOCATION_LENGTH = 28;
    static final int MIN_DESC_LENGTH = 3;
    static final int MAX_DESC_LENGTH = 151;
    static final String API_KEY = "Your API Key";
    static final String FTP_USER = "Your Ftp User";
    static final String FTP_PASS = "Your FTP Pass";
    static final String FTP_HOST = "Your FTP Host";
    static final String HOST = "Your Web Host";
}
